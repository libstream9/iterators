#ifndef STREAM9_ITERATORS_COUNTING_ITERATOR_HPP
#define STREAM9_ITERATORS_COUNTING_ITERATOR_HPP

#include "iterator_facade.hpp"

#include <concepts>
#include <iterator>

namespace stream9::iterators {

namespace iter = iterators;

template<std::integral T>
class counting_iterator : public iter::iterator_facade<
                                    counting_iterator<T>,
                                    std::random_access_iterator_tag,
                                    T, // reference
                                    T, // value_type
                                    T  // difference_type
                                 >
{
public:
    counting_iterator() = default;

    counting_iterator(T const v)
        : m_value { v }
    {}

private:
    friend class iter::iterator_core_access;

    T dereference() const
    {
        return m_value;
    }

    void increment()
    {
        ++m_value;
    }

    void decrement()
    {
        --m_value;
    }

    void advance(std::iter_difference_t<counting_iterator> const n)
    {
        m_value += n;
    }

    std::iter_difference_t<counting_iterator>
        distance_to(counting_iterator const& other) const
    {
        return other.m_value - m_value;
    }

    bool equal(counting_iterator const& other) const
    {
        return other.m_value == m_value;
    }

    bool equal(std::default_sentinel_t) const
    {
        return false;
    }

    std::strong_ordering
        compare(counting_iterator const& other) const
    {
        return other.m_value <=> m_value;
    }

    std::strong_ordering
        compare(std::default_sentinel_t) const
    {
        return std::strong_ordering::less;
    }

private:
    T m_value {};
};

} // namespace stream9::iterators

#endif // STREAM9_ITERATORS_COUNTING_ITERATOR_HPP
