#ifndef STREAM9_ITERATORS_ITERATOR_FACADE_HPP
#define STREAM9_ITERATORS_ITERATOR_FACADE_HPP

#include "type_traits.hpp"

#include "iterator_facade/iterator_core_access.hpp"

#include <concepts>
#include <iterator>

namespace stream9::iterators {

struct iterator_facade_base {};

template<typename Derived,
         typename Category,
         typename Reference,
         typename Value = std::remove_reference_t<Reference>,
         typename Difference = std::ptrdiff_t >
    requires iterator_category_tag<Category>
          && (!std::is_reference_v<Value>)
          && std::signed_integral<Difference>
class iterator_facade : public iterator_facade_base
{
public:
    using iterator_category = Category;
    using value_type = Value;
    using reference = Reference;
    using difference_type = Difference;
    using pointer = std::conditional_t<std::is_reference_v<reference>,
                        std::add_pointer_t<std::remove_reference_t<reference>>,
                        void >;

private:
    template<typename Tag>
    static constexpr bool is_derived_from_category_v =
        std::derived_from<iterator_category, Tag>;

public:
    constexpr iterator_facade() noexcept
    {
        check_requirement();
    }

    constexpr decltype(auto)
    operator*() const
        requires requires (Derived& i) {
                    iterator_core_access::dereference(i);
                 }
    {
        return iterator_core_access::dereference(derived());
    }

    constexpr decltype(auto)
    operator[](difference_type const n) const
        requires is_derived_from_category_v<std::random_access_iterator_tag>
    {
        return *(derived() + static_cast<difference_type>(n));
    }

    constexpr auto
    operator->() const
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && std::is_reference_v<reference>
    {
        return &operator*();
    }

    constexpr Derived&
    operator++()
        requires requires (Derived& i) {
                    iterator_core_access::increment(i);
                 }
    {
        iterator_core_access::increment(derived());

        return derived();
    }

    constexpr Derived
    operator++(int)
        requires requires (Derived& i) {
                    iterator_core_access::increment(i);
                 }
    {
        auto tmp = derived();
        ++*this;
        return tmp;
    }

    constexpr Derived&
    operator--()
        requires is_derived_from_category_v<std::bidirectional_iterator_tag>
              && requires (Derived& i) {
                  iterator_core_access::decrement(i);
              }
    {
        iterator_core_access::decrement(derived());

        return derived();
    }

    constexpr Derived
    operator--(int)
        requires is_derived_from_category_v<std::bidirectional_iterator_tag>
              && requires (Derived& i) {
                  iterator_core_access::decrement(i);
              }
    {
        auto tmp = derived();
        --*this;
        return tmp;
    }

    friend constexpr Derived
    operator+(Derived const& it, difference_type const n)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, n);
              }
    {
        auto tmp = it;
        tmp += n;
        return tmp;
    }

    friend constexpr Derived
    operator+(difference_type const n, Derived const& it)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, n);
              }
    {
        return operator+(it, n);
    }

    constexpr Derived&
    operator+=(difference_type const n)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, n);
              }
    {
        iterator_core_access::advance(derived(), n);

        return derived();
    }

    friend constexpr difference_type
    operator-(Derived const& lhs, Derived const& rhs)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived const& i, Derived const& j) {
                  iterator_core_access::distance_to(i, j);
              }
    {
        return iterator_core_access::distance_to(rhs, lhs);
    }

    friend constexpr Derived
    operator-(Derived const& it, difference_type const n)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, -n);
              }
    {
        auto tmp = it;
        tmp -= n;
        return tmp;
    }

    constexpr Derived&
    operator-=(difference_type const n)
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type const n) {
                  iterator_core_access::advance(i, -n);
              }
    {
        iterator_core_access::advance(derived(), -n);

        return derived();
    }

    template<typename T>
        requires is_derived_from_category_v<std::input_iterator_tag>
              && requires (Derived const& lhs, T const& rhs) {
                  iterator_core_access::equal(lhs, rhs);
              }
    friend constexpr bool
    operator==(Derived const& lhs, T const& rhs)
    {
        return iterator_core_access::equal(lhs, rhs);
    }

    template<typename T>
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived const& lhs, T const& rhs) {
                  iterator_core_access::compare(lhs, rhs);
              }
    friend constexpr std::strong_ordering
    operator<=>(Derived const& lhs, T const& rhs)
    {
        return iterator_core_access::compare(lhs, rhs);
    }

private:
    constexpr Derived&
    derived()
    {
        return static_cast<Derived&>(*this);
    }

    constexpr Derived const&
    derived() const
    {
        return static_cast<Derived const&>(*this);
    }

    static constexpr void check_requirement() noexcept
    {
        if constexpr (is_derived_from_category_v<std::input_iterator_tag>) {
            static_assert(requires (Derived& d) {
                { iterator_core_access::dereference(d) }
                                    -> std::same_as<Reference>;
            });

            static_assert(requires (Derived& d) {
                iterator_core_access::increment(d);
            });
        }
        if constexpr (is_derived_from_category_v<std::forward_iterator_tag>) {
            static_assert(requires (Derived& d) {
                { iterator_core_access::equal(d, d) } -> std::same_as<bool>;
            });
        }
        if constexpr (is_derived_from_category_v<std::bidirectional_iterator_tag>) {
            static_assert(requires (Derived& d) {
                iterator_core_access::decrement(d);
            });
        }
        if constexpr (is_derived_from_category_v<std::random_access_iterator_tag>) {
            static_assert(requires (Derived& d, Difference n) {
                iterator_core_access::advance(d, n);
            });
            static_assert(requires (Derived const& d1, Derived const& d2) {
                { iterator_core_access::distance_to(d1, d2) }
                                                -> std::same_as<Difference>;
            });
            static_assert(requires (Derived const& d1, Derived const& d2) {
                { iterator_core_access::compare(d1, d2) }
                            -> std::same_as<std::strong_ordering>;
            });
        }
    }
};

} // namespace stream9::iterators

namespace std {

template<std::derived_from<stream9::iterators::iterator_facade_base> T>
struct pointer_traits<T>
{
    static auto
    to_address(T i)
    {
        return i.operator->();
    }
};

} // namespace std

#endif // STREAM9_ITERATORS_ITERATOR_FACADE_HPP
