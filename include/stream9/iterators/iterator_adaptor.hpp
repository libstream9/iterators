#ifndef STREAM9_ITERATORS_ITERATOR_ADAPTOR_HPP
#define STREAM9_ITERATORS_ITERATOR_ADAPTOR_HPP

#include "iterator_facade.hpp"
#include "type_traits.hpp"

#include <concepts>
#include <iterator>

namespace stream9::iterators {

/*
 * @model std::semiregular
 * @model std::input_iterator
 */
template<typename Derived,
         std::input_iterator Base,
         typename Category = std::iterator_traits<Base>::iterator_category,
         typename Reference = std::iterator_traits<Base>::reference,
         typename Value = std::remove_reference_t<Reference>,
         typename Difference = std::iterator_traits<Base>::difference_type >
    requires iterator_category_tag<Category>
          && (!std::is_reference_v<Value>)
          && std::signed_integral<Difference>
class iterator_adaptor : public iterator_facade<
                               Derived, Category, Reference, Value, Difference>
{
protected:
    using adaptor_type = iterator_adaptor<
                        Derived, Base, Category, Reference, Value, Difference>;
public:
    // essentials
    iterator_adaptor() = default;

    iterator_adaptor(Base const& it) noexcept
        : m_base { it }
    {}

    Base const& base() const noexcept { return m_base; }
    Base&       base() noexcept { return m_base; }

private:
    friend class iterator_core_access;

    auto& dereference() const noexcept
    {
        return *m_base;
    }

    void increment() noexcept
    {
        ++m_base;
    }

    void decrement() noexcept
        requires std::bidirectional_iterator<Base>
    {
        --m_base;
    }

    void advance(Base::difference_type const n) noexcept
        requires std::random_access_iterator<Base>
    {
        m_base += n;
    }

    Base::difference_type distance_to(iterator_adaptor const other) const noexcept
        requires std::random_access_iterator<Base>
    {
        return other.m_base - m_base;
    }

    bool equal(iterator_adaptor const other) const noexcept
        requires std::equality_comparable<Base>
    {
        return m_base == other.m_base;
    }

    auto compare(iterator_adaptor const other) const noexcept
        requires std::totally_ordered<Base>
    {
        if (m_base < other.m_base) {
            return std::strong_ordering::less;
        }
        else if (m_base > other.m_base) {
            return std::strong_ordering::greater;
        }
        else {
            return std::strong_ordering::equal;
        }
    }

private:
    Base m_base;
};

} // namespace stream9::iterators

#endif // STREAM9_ITERATORS_ITERATOR_ADAPTOR_HPP
